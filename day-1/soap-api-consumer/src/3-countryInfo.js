const axios = require("axios");
const { xmlParser, writeFile } = require("./utils");

const api =
  "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso";

const country = "US";

const data = `<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
            <CapitalCity xmlns="http://www.oorsprong.org/websamples.countryinfo">
                <sCountryISOCode>${country}</sCountryISOCode>
            </CapitalCity>
        </soap:Body>
    </soap:Envelope>
    `;

const config = {
  method: "post",
  maxBodyLength: Infinity,
  url: api,
  headers: {
    "Content-Type": "text/xml",
  },
  data: data,
};

axios(config)
  .then(function (response) {
    const { data } = response;
    xmlParser(data).then((json) => {
      console.log(
        "Capital of " +
          country +
          " is : " +
          json["soap:Envelope"]["soap:Body"][0]["m:CapitalCityResponse"][0][
            "m:CapitalCityResult"
          ][0]
      );
      writeFile("3-countryInfo", json);
    });
  })
  .catch(function (error) {
    console.log(error);
  });
