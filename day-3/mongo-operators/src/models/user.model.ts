import mongoose from "mongoose";
import { IUser } from "../interface";

const userSchema = new mongoose.Schema<IUser>({
  email: String,
  username: String,
  password: String,
  age: Number  
});

export default mongoose.model<IUser>("user-collection", userSchema);
