import jwt from "jsonwebtoken";
import { Request, Response, NextFunction } from "express";

const jwtSecretKey: string = process.env.JWT_SECRET_KEY || "secret@123";

const authVerify = (req: Request, res: Response, next: NextFunction) => {
  try {
    const token = req.headers.authorization?.split(" ")[1];

    if (!token) {
      return res.status(401).json({
        message: "Unauthorized",
      });
    }

    jwt.verify(token, jwtSecretKey, (err: Error | any, decoded: any) => {
      if (err) {
        return res.status(401).json({
          message: "Unauthorized",
        });
      }

      //   console.log("🗝️ Decoded data :", decoded);

      next();
      return;
    });
  } catch (err: Error | any) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving posts.",
    });
  }
};

export default authVerify;
