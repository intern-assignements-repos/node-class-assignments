import { Response, Request } from "express";

import User from "../../models/user.model";
import queryOperators from "./queryOperators";
import updateOperators from "./updateOperators";

class UserController {
  public static async queryFunction(req: Request, res: Response) {
    try {
      const key = Object.keys(req.query)[0] as string;

      if (key) queryOperators(req, res);
      else
        return res.send({
          message: "api hit successfully!",
        });

      return;
    } catch (err: Error | any) {
      console.error(err);
      return res.status(500).send({
        message: err?.message || "Some error occurred.",
      });
    }
  }

  public static async updateFunction(req: Request, res: Response){
    try {
    } catch (err: Error | any) {
      console.error(err);
      return res.status(500).send({
        message: err?.message || "Some error occurred.",
      });
    }
  }

  public static async signUpFunction(req: Request, res: Response) {
    try {
    } catch (err: Error | any) {
      console.error(err);
      return res.status(500).send({
        message: err?.message || "Some error occurred.",
      });
    }
  }


}

export default UserController;
