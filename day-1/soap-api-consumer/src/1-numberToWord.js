const axios = require("axios");
const { xmlParser, writeFile } = require("./utils");

const api = "https://www.dataaccess.com/webservicesserver/NumberConversion.wso";

const numberToConvert = 12345;

const data = `
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
      <ubiNum>${numberToConvert}</ubiNum>
    </NumberToWords>
  </soap:Body>
</soap:Envelope>
`;

const config = {
  method: "post",
  maxBodyLength: Infinity,
  url: api,
  headers: {
    "Content-Type": "text/xml",
  },
  data: data,
};

axios(config)
  .then(function (response) {
    const { data } = response;
    xmlParser(data).then((json) => {
      console.log(
        json["soap:Envelope"]["soap:Body"][0]["m:NumberToWordsResponse"][0][
          "m:NumberToWordsResult"
        ][0]
      );
      writeFile("1-numberToWord", json);
    });
  })
  .catch(function (error) {
    console.log(error);
  });
