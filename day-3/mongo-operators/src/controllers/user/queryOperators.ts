import { Request, Response } from "express";
import { IUser } from "../../interface";

import User from "../../models/user.model";

const queryOperators = async (req: Request, res: Response) => {
  try {
    const key = Object.keys(req.query)[0] as string;
    const val = req.query[key] as string;
    switch (key) {
      case "eq": {
        const data: IUser[] = await User.find({ age: { $eq: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "gt": {
        const data: IUser[] = await User.find({ age: { $gt: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "gte": {
        const data: IUser[] = await User.find({ age: { $gte: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "lt": {
        const data: IUser[] = await User.find({ age: { $lt: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "lte": {
        const data: IUser[] = await User.find({ age: { $lte: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "ne": {
        const data: IUser[] = await User.find({ age: { $ne: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "ne": {
        const data: IUser[] = await User.find({ age: { $ne: val } });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      case "in": {
        const val2 = req.query.val2 ;
        console.log("Val :",val2);
        const data: IUser[] = await User.find({ age: { $in: [val, val2] } }, { _id: 0 });
        return res.send({
          message: `Key: ${key}; Value: ${val}`,
          data: data,
        });
      }
      default: {
        return res.send({
          message: "Query api hit successfully!",
        });
      }
    }
  } catch (err: Error | any) {
    console.error(err);
    return res.status(500).send({
      message: err?.message || "Some error occurred.",
    });
  }
};

export default queryOperators;
