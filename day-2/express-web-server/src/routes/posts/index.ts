import express from "express";

import PostController from "../../controllers/PostController";
import authVerify from "../../middlewares/auth";

const postRouter = express.Router();

postRouter.get("/", authVerify, PostController.getAllPosts);
postRouter.get("/:id", authVerify, PostController.getPostById);
postRouter.post("/", authVerify, PostController.createPost);
postRouter.put("/:id", authVerify, PostController.updatePost);
postRouter.delete("/:id", authVerify, PostController.deletePost);
postRouter.put("/:id/like", authVerify, PostController.likePost);
postRouter.put("/:id/unlike", authVerify, PostController.unlikePost);
postRouter.put("/:id/comment", authVerify, PostController.commentPost);

export default postRouter;
