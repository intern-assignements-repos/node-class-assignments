const axios = require("axios");
const cheerio = require("cheerio");
const pretty = require("pretty");
const fs = require("fs").promises;

const govtSchoolUrl =
  "https://school.careers360.com/schools/government-schools-in-chandigarh";

const privateSchoolUrl =
  "https://school.careers360.com/schools/private-schools-in-chandigarh";

const scrapeData = async (api) => {
  try {
    const { data } = await axios.get(api);
    const $ = cheerio.load(data);
    await fs.writeFile("./out/index.html", pretty($.html()), (err) => {
      if (err) {
        console.log(err);
      }
    });

    const schools = [];
    $(".cardBlkInn").each((i, el) => {
      const schoolList = $(el).find(".schoolList");

      schools.push(
        schoolList
          .find(".schoolCon")
          .children(".schoolConright")
          .children(".schoolPoint ")
          .children(".headingBox")
          .children(".blockHeading")
          .children("a")
          .text()
          .trim()
      );
    });

    schools.forEach((school) => {
      console.log(school);
    });
  } catch (error) {
    console.log(error);
  }
};

scrapeData(govtSchoolUrl);
scrapeData(privateSchoolUrl);