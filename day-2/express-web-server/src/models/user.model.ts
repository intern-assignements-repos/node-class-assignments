import mongoose from "mongoose";
import { IUser } from "../interfaces";

const userSchema = new mongoose.Schema<IUser>({
  emailId: String,
  userName: String,
  password: String,
  phone: String,
  createdOn: {
    type: Date,
    default: () => Date.now(),
  },
});

export default mongoose.model<IUser>("user-collection", userSchema);
