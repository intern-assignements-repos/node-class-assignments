import mongoose from "mongoose";
import { IPost } from "../interfaces";

const postSchema = new mongoose.Schema<IPost>({
  postedBy: {
    type: String,
    required: true,
  },
  title: String,
  body: String,
  images: [
    {
      type: String,
      default: "",
    },
  ],
  likes: {
    type: Number,
    default: 0,
  },
  unlikes: {
    type: Number,
    default: 0,
  },
  comments: [
    {
      comment: String,
      commentBy: String,
      commentOn: Date,
      commentId: String,
      commentLikes: Number,
      commentUnlikes: Number,
    },
  ],
  createdOn: {
    type: Date,
    default: () => Date.now(),
  },
});

export default mongoose.model<IPost>("post-collection", postSchema);
