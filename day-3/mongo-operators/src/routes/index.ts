import { Router } from "express";
import UserController from "../controllers/user";

const userRouter = Router();

userRouter.get("/", UserController.queryFunction);

export default userRouter;
