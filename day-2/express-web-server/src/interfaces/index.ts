import { Date } from "mongoose";

export interface IUser {
  emailId: string;
  userName: string;
  phone: string;
  password: string;
  createdOn: Date;
}

export type ILoginCred = Pick<IUser, "userName" | "password">;

export interface IComment {
  comment: string;
  commentBy: string;
  commentId: string;
  commentLikes: number;
  commentUnlikes: number;
}

export interface IPost {
  postedBy: string;
  title: string;
  body: string;
  images: string[];
  likes: number;
  unlikes: number;
  comments: IComment[];
  createdOn: Date;
}
