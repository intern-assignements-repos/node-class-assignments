import { Router } from "express";
import UserController from "../../controllers/UserController";

const userRouter = Router();

userRouter.post("/signUp", UserController.signUp);
userRouter.post("/signIn", UserController.signIn);
userRouter.post("/forgot-password", UserController.forgotPassword);

export default userRouter;
