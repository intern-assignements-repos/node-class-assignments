const axios = require("axios");
const { xmlParser, writeFile } = require("./utils");

const temp = 100;

const data = `<?xml version="1.0" encoding="utf-8"?>
  <soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
    <soap12:Body>
      <CelsiusToFahrenheit xmlns="https://www.w3schools.com/xml/">
        <Celsius>${temp}</Celsius>
      </CelsiusToFahrenheit>
    </soap12:Body>
  </soap12:Envelope>
  `;

const config = {
  method: "post",
  maxBodyLength: Infinity,
  url: "https://www.w3schools.com/xml/tempconvert.asmx",
  headers: {
    "Content-Type": "text/xml",
  },
  data: data,
};

axios(config)
  .then(function (response) {
    const { data } = response;
    xmlParser(data).then((json) => {
      console.log(
        json["soap:Envelope"]["soap:Body"][0]["CelsiusToFahrenheitResponse"][0][
          "CelsiusToFahrenheitResult"
        ][0] + "°F"
      );
      writeFile("4-w3c", json);
    });
  })
  .catch(function (error) {
    console.log(error);
  });
