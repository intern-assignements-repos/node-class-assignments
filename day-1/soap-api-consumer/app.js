const soapRequest = require("easy-soap-request");

const { xmlParser, writeFile } = require("./src/utils");

const api = "https://www.dataaccess.com/webservicesserver/NumberConversion.wso";

const numberToConvert = 12345;

const bodySample = `
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Body>
        <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
        <ubiNum>${numberToConvert}</ubiNum>
        </NumberToWords>
    </soap:Body>
    </soap:Envelope>
`;

const headersSample = {
  "Content-Type": "text/xml;charset=UTF-8",
};

(async (api, headersSample, bodySample) => {
  const { response } = await soapRequest({
    method: "POST",
    url: api,
    headers: headersSample,
    xml: bodySample,
  });
  const { headers, body, statusCode } = response;
  const json = await xmlParser(body);
  await writeFile("out", json);
})(api, headersSample, bodySample);
