const axios = require("axios");
const { xmlParser, writeFile } = require("./utils");

const api =
  "http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso";


const data = `<?xml version="1.0" encoding="utf-8"?>
<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
  <soap12:Body>
    <ListOfCountryNamesByName xmlns="http://www.oorsprong.org/websamples.countryinfo">
    </ListOfCountryNamesByName>
  </soap12:Body>
</soap12:Envelope>

    `;

const config = {
  method: "post",
  maxBodyLength: Infinity,
  url: api,
  headers: {
    "Content-Type": "text/xml",
  },
  data: data,
};

axios(config)
  .then(function (response) {
    const { data } = response;
    xmlParser(data).then((json) => {
      const countries =
        json["soap:Envelope"]["soap:Body"][0][
          "m:ListOfCountryNamesByNameResponse"
        ][0]["m:ListOfCountryNamesByNameResult"][0]["m:tCountryCodeAndName"];
      console.log(
        "All Countries: " +
          countries.map((country) => country["m:sName"][0]).join("\n ")
      );
      writeFile("2-countryInfo", json);
    });
  })
  .catch(function (error) {
    console.log(error);
  });
