const xml2js = require("xml2js");
const fs = require("fs").promises;

const xmlParser = (xml) => {
  return new Promise((resolve, reject) => {
    xml2js.parseString(xml, (err, result) => {
      if (err) {
        reject(err);
      }
      resolve(result);
    });
  });
};

const writeFile = async (fileName, data) => {
  try {
    await fs.writeFile(`../out/${fileName}.json`, JSON.stringify(data, null, 2));
  } catch (err) {
    console.error(err);
  }
};

module.exports = {
  xmlParser,
  writeFile,
};
