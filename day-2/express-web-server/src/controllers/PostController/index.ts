import { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";

import Post from "../../models/post.model";
import User from "../../models/user.model";
import { IPost, IUser, IComment } from "../../interfaces";

class PostController {
  public static async getAllPosts(req: Request, res: Response) {
    try {
      const posts = await Post.find();

      if (!posts) {
        res.status(404).send({
          message: `Posts not found`,
        });

        return;
      }

      res.send(posts);
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving posts.",
      });
    }
  }

  public static async getPostById(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const post = await Post.findById(id);

      if (!post) {
        res.status(404).send({
          message: `Post with id ${id} not found`,
        });

        return;
      }

      res.send(post);
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving posts.",
      });
    }
  }

  public static async createPost(req: Request, res: Response) {
    try {
      if (!req.body) {
        res.status(400).json({
          message: "Content can not be empty!",
        });

        return;
      }

      const input: IPost = req.body;

      const { postedBy, title, body, images } = input;

      if (!postedBy) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const user = User.find({ userName: postedBy });

      if (!user) {
        res.status(404).send({
          message: `User with userName ${postedBy} not found`,
        });

        return;
      }

      if (!title || !body) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const post = new Post({
        postedBy,
        title,
        body,
        images,
        createdOn: new Date(),
      });

      const data = await post.save();

      res.send({
        message: "Post created successfully",
        data,
      });
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Post.",
      });

      return;
    }
  }

  public static async updatePost(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const post = await Post.findById(id);

      if (!post) {
        res.status(404).send({
          message: `Post with id ${id} not found`,
        });

        return;
      }

      const input: IPost = req.body;

      const { title, body, images } = input;

      if (!title || !body) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const updatedPost = await Post.findByIdAndUpdate(id, {
        title,
        body,
        images,
      });

      res.send({
        message: `Post with id ${id} updated successfully`,
        data: updatedPost,
      });
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while updating the Post.",
      });

      return;
    }
  }

  public static async deletePost(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const post = await Post.findById(id);

      if (!post) {
        res.status(404).send({
          message: `Post with id ${id} not found`,
        });

        return;
      }

      const deletedPost = await Post.findByIdAndDelete(id);

      res.send({
        message: `Post with id ${id} deleted successfully`,
        data: deletedPost,
      });
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while deleting the Post.",
      });

      return;
    }
  }

  public static async likePost(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const post = await Post.findById(id);

      if (!post) {
        res.status(404).send({
          message: `Post with id ${id} not found`,
        });

        return;
      }

      const { userId } = req.body;

      if (!userId) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const likes = post.likes + 1;

      const updatedPost = await Post.findByIdAndUpdate(id, {
        likes: likes,
      });

      res.send({
        message: `Post with id ${id} liked successfully`,
        data: updatedPost,
      });
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while liking the Post.",
      });

      return;
    }
  }

  public static async unlikePost(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const post = await Post.findById(id);

      if (!post) {
        res.status(404).send({
          message: `Post with id ${id} not found`,
        });

        return;
      }

      const { userId } = req.body;

      if (!userId) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const unlikes = post.unlikes + 1;

      const updatedPost = await Post.findByIdAndUpdate(id, {
        unlikes: unlikes,
      });

      res.send({
        message: `Post with id ${id} unliked successfully`,
        data: updatedPost,
      });
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while unliking the Post.",
      });

      return;
    }
  }

  public static async commentPost(req: Request, res: Response) {
    try {
      const id = req.params.id;
      const post = await Post.findById(id);

      if (!post) {
        res.status(404).send({
          message: `Post with id ${id} not found`,
        });

        return;
      }

      const { comment, commentBy, commentLikes, commentUnlikes } =
        req.body as IComment;

      if (!comment || !commentBy) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const comments = post.comments;

      const newComment: IComment = {
        comment,
        commentBy,
        commentLikes,
        commentUnlikes,
        commentId: uuidv4(),
      };

      comments.push(newComment);

      const updatedPost = await Post.findByIdAndUpdate(id, {
        comments: comments,
      });

      res.send({
        message: `Post with id ${id} commented successfully`,
        data: updatedPost,
      });
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message:
          err.message || "Some error occurred while commenting the Post.",
      });

      return;
    }
  }
}

export default PostController;
