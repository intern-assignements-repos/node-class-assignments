import express from "express";
import mongoose, { ConnectOptions } from "mongoose";
import bodyParser from "body-parser";

import { userRouter, postRouter } from "./src/routes";

const app = express();
const port = process.env.PORT || 6000;
const host = process.env.HOST || "localhost";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const dbUrl = "mongodb://127.0.0.1:27017/";
const dbName = "express-node-blog";

mongoose.set("strictQuery", false);
mongoose
  .connect(dbUrl + dbName, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  } as ConnectOptions)
  .then(() => console.log(`🐒🛢️ 🔗  MongoDB connected to ${dbUrl}${dbName}`))
  .catch((err) => console.error(err));

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.use("/api/user", userRouter);
app.use("/api/post", postRouter);

app.listen(port, () => {
  return console.log(
    `🚂🚃🚃🚃 Express server is running at http://${host}:${port}`
  );
});
