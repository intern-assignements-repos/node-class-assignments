import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";
import { Request, Response } from "express";

import User from "../../models/user.model";
import { IUser, ILoginCred } from "../../interfaces";

const jwtSecretKey: string = process.env.JWT_SECRET_KEY || "secret@123";

const smtpHost = "smtp.ethereal.email";
const smtpPort = 587;
const smtpUser = "leonard14@ethereal.email";
const smtpPass = "UmYjpwheQZXGAusvuC";

const senderEmail = "wits@email.com";

const sendEmail = async (email: string) => {
  try {
    console.log("Sending Email to :", email);

    const transporter = nodemailer.createTransport({
      host: smtpHost,
      port: smtpPort,
      auth: {
        user: smtpUser,
        pass: smtpPass,
      },
    });

    const subject = "Forgot Password";
    const text = "Reset link for your account!";

    await transporter.sendMail({
      from: senderEmail,
      to: email,
      subject: subject,
      text: text,
    });

    console.log("Mail sent successfully!");

    return;
  } catch (err: Error | any) {
    console.error("Mailer Error : ", err);
  }
};

class UserController {
  public static async signUp(req: Request, res: Response) {
    try {
      if (!req.body) {
        res.status(400).json({
          message: "Content can not be empty!",
        });

        return;
      }

      const input: IUser = req.body;

      const { phone, emailId, password, userName } = input;

      console.log(phone, emailId, password, userName);

      if (!phone || !emailId || !password || !userName) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const user = new User({
        phone,
        emailId,
        password,
        userName,
        createdOn: new Date(),
      });

      const data = await user.save();

      res.send(data);

      res.send("Hello World!");
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });

      return;
    }
  }

  public static async signIn(req: Request, res: Response) {
    try {
      if (!req.body) {
        res.status(400).json({
          message: "Content can not be empty!",
        });

        return;
      }

      const input: ILoginCred = req.body;
      const { userName, password } = input;

      if (!userName || !password) {
        res.status(400).json({
          message: "Please provide all the required fields",
        });

        return;
      }

      const user = await User.findOne({
        userName,
        password,
      });

      if (!user) {
        res.status(400).json({
          message: "Invalid credentials",
        });

        return;
      }

      const token = jwt.sign(
        {
          userName: user.userName,
          emailId: user.emailId,
        },
        jwtSecretKey,
        {
          expiresIn: "1h",
        }
      );

      res.send({
        message: "Login successful",
        token,
      });

      return;
    } catch (err: Error | any) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });

      return;
    }
  }

  public static async forgotPassword(req: Request, res: Response) {
    try {
      if (!req.body)
        return res.status(400).json({
          message: "Content can not be empty!",
        });

      const { email } = req.body;

      if (email) {
        const userExist = await User.findOne({
          emailId: email,
        });

        if (userExist) await sendEmail(email);
        else {
          return res.status(400).json({
            message: "User with this Email id not Exist!",
          });
        }
      } else {
        return res.status(400).json({
          message: "Content can not be empty!",
        });
      }

      return res.send({
        message: `Mail sent to ${email} successfully!`,
      });
    } catch (err: Error | any) {
      console.error(err);
      return res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });
    }
  }
}

export default UserController;
